#!/bin/sh
echo -e "apiVersion: v1 \n\
clusters:\n\
- cluster:\n\
    certificate-authority-data: ${KUBE_CLUSTER_CERT_DATA}\n\
    server: ${KUBE_API}\n\
  name: kubernetes.macrosoft.se\n\
contexts:\n\
- context:\n\
    cluster: kubernetes.macrosoft.se\n\
    user: kubernetes-admin\n\
  name: macrosoft.se\n\
current-context: macrosoft.se\n\
kind: Config\n\
preferences: {}\n\
users:\n\
- name: kubernetes-admin\n\
  user:\n\
    client-certificate-data: ${KUBE_CLIENT_CERT_DATA}\n\
    client-key-data: ${KUBE_CLIENT_KEY_DATA}\n"\
> /config/.kube/config