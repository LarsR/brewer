FROM alpine:3.7

RUN mkdir -p /opt/brewer/bin
COPY brewer /opt/brewer/bin/
COPY templates /opt/brewer/templates
COPY migrations /opt/brewer/migrations
ENV DB_HOSTNAME mysql
ENV DB_USERNAME root
ENV DB_DATABASE brewer
ENV TEMPLATE-PATH /opt/brewer/templates
ENV MIGRATIONS /opt/brewer/migrations
CMD ["/opt/brewer/bin/brewer"]

