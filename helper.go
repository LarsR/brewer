package main

import (
	"log"
	"os"
	"path/filepath"
	"strings"
)

// PathToFullPath Return full path based on executable path
func PathToFullPath(inPath string) string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Println(err)
		return strings.TrimRight(inPath, "/")
	}
	if !strings.HasPrefix(inPath, "/") {
		inPath = dir + "/" + inPath
	}
	return strings.TrimRight(inPath, "/")
}
