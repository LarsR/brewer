package mysql_test

import (
	"testing"

	"gitlab.com/LarsR/brewer/database/mysql"
	"gitlab.com/LarsR/brewer/testhelper"
)

func TestMigrationInvalidFile(t *testing.T) {
	f := "/fullpath/to/folder"
	err := mysql.Migrate(f)
	testhelper.NotOk(t, err)
}
