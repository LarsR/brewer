package mysql

import (
	"database/sql"
	"io/ioutil"
	"log"
	"strings"
	"time"

	// Driver to mysql
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/LarsR/brewer/brewing"
)

var (
	// DataSourceName connection string for database
	DataSourceName string
	connection     *sql.DB
)

// DRIVER name of database
const (
	DATEFORMAT = "2006-01-02 15:04:05"
	DRIVER     = "mysql"
)

// Migrate form file
func Migrate(file string) error {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}
	err = connect()

	if err != nil {
		return err
	}
	defer connection.Close()
	sqls := strings.Split(string(data), ";")
	for _, sql := range sqls {
		sql = strings.Trim(sql, "\n")
		if sql != "" {
			_, err := connection.Query(sql)
			if err != nil {
				log.Print("Query-Error: ", err)
			}
		}
	}
	return err
}

// GetBrewings return a list of all brewings
func GetBrewings() ([]brewing.Brewing, error) {
	bs := []brewing.Brewing{}
	err := connect()
	if err != nil {
		return bs, err
	}
	defer connection.Close()

	rows, err := connection.Query("SELECT * FROM brewings")
	if err != nil {
		return bs, err
	}
	for rows.Next() {
		var b brewing.Brewing
		var d, u string
		err = rows.Scan(&b.UID, &b.Name, &b.Description, &b.Notes, &b.Ingredients, &b.OG, &b.FG, &b.IBU, &d, &u, &b.BrewingDay)
		t, _ := time.Parse(DATEFORMAT, d)
		t2, _ := time.Parse(DATEFORMAT, u)
		b.Created = t
		b.Updated = t2
		bs = append(bs, b)
	}
	return bs, err
}

// AddBrewing Add new brewing to database
func AddBrewing(b *brewing.Brewing) error {
	err := connect()
	if err != nil {
		return err
	}
	defer connection.Close()
	stmtIns, err := connection.Prepare("INSERT INTO brewings SET name=?, description=? ,notes=?, ingredients=?, og=?, fg=?, ibu=?, created=?, updated=?, brewingday=?")
	if err != nil {
		return err
	}
	defer stmtIns.Close()
	dt := time.Now().Format(DATEFORMAT)
	res, err := stmtIns.Exec(b.Name, b.Description, b.Notes, b.Ingredients, b.OG, b.FG, b.IBU, dt, dt, b.BrewingDay)
	if err != nil {
		return err
	}
	i, err := res.LastInsertId()
	if err != nil {
		return err
	}
	b.UID = int(i)
	return err
}

// UpdateBrewing update brewing
func UpdateBrewing(b *brewing.Brewing) error {
	err := connect()
	if err != nil {
		return err
	}
	defer connection.Close()
	stmtIns, err := connection.Prepare("UPDATE brewings SET name=?, description=?, notes=?, ingredients=?, og=?, fg=?, ibu=?, updated=?, brewingday=? WHERE uid=?")
	if err != nil {
		return err
	}
	defer stmtIns.Close()
	dt := time.Now().Format(DATEFORMAT)
	_, err = stmtIns.Exec(b.Name, b.Description, b.Notes, b.Ingredients, b.OG, b.FG, b.IBU, dt, b.BrewingDay, b.UID)
	return err
}

// Unexported methods
func connect() error {
	var err error
	connection, err = sql.Open(DRIVER, DataSourceName)
	if err != nil {
		return err
	}
	// err = connection.Ping()
	// if err != nil {
	// 	return err
	// }
	return nil
}
