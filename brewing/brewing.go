package brewing

import (
	"time"
)

// Brewing contains info about a single brew
type Brewing struct {
	UID         int       `json:"uid"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	Notes       string    `json:"notes"`
	Ingredients string    `json:"ingredients"`
	OG          float64   `json:"og"`
	FG          float64   `json:"fg"`
	IBU         float64   `json:"ibu"`
	BrewingDay  string    `json:"brewingday"`
	Created     time.Time `json:"created"`
	Updated     time.Time `json:"updated"`
}
