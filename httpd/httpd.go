package httpd

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/LarsR/brewer/brewing"
	"gitlab.com/LarsR/brewer/database/mysql"

	uuid "github.com/satori/go.uuid"
)

//Session Layout of login session
type Session struct {
	GUID    string
	Timeout *time.Time
}

// Httpd Container for httpd server
type Httpd struct {
	templates *template.Template
	Sessions  []*Session
	menuItems []MenuItem
	root      string
}

// MenuItem gui menu item
type MenuItem struct {
	URI      string
	Title    string
	Selected bool
}

var (
	timeout time.Duration = time.Hour
)

type RequestData struct {
	Title     string
	MenuItems []MenuItem
	Data      interface{}
	LoggedIn  bool
}

// New Httpd Server
func New(templates string) *Httpd {
	h := new(Httpd)
	h.templates = template.Must(template.ParseGlob(templates + "/html/*.html"))
	h.root = templates
	h.menuItems = append(h.menuItems, MenuItem{"/brewings", "Bryggningar", true})
	h.menuItems = append(h.menuItems, MenuItem{"/on-tap", "På Tapp", false})
	h.menuItems = append(h.menuItems, MenuItem{"/tools", "Verktyg", false})
	http.Handle("/brewings/", http.HandlerFunc(h.brewings))
	http.Handle("/brewings/add", http.HandlerFunc(h.addBrewing))
	http.Handle("/on-tap", http.HandlerFunc(h.onTap))
	http.Handle("/", http.HandlerFunc(h.getTemplate))
	go func() {
		http.ListenAndServe(":8000", nil)
	}()
	return h
}

func (h Httpd) menu(selected string) []MenuItem {
	for _, mi := range h.menuItems {
		mi.Selected = mi.URI == selected
	}
	return h.menuItems
}

func (h *Httpd) brewings(w http.ResponseWriter, r *http.Request) {
	h.templates = template.Must(template.ParseGlob(h.root + "/html/*.html"))
	rd := new(RequestData)
	rd.Title = "Bryggboken"
	rd.MenuItems = h.menu("/brewings")

	err := h.templates.ExecuteTemplate(w, "brewings.html", rd)
	if err != nil {
		log.Print("Cannot Get View ", err)
	}
}

func (h *Httpd) addBrewing(w http.ResponseWriter, r *http.Request) {
	h.templates = template.Must(template.ParseGlob(h.root + "/html/*.html"))
	if r.FormValue("name") != "" {
		var b brewing.Brewing
		var err error
		b.Name = r.FormValue("name")
		b.Ingredients = r.FormValue("ingredients")
		b.Description = r.FormValue("description")
		b.Notes = r.FormValue("notes")
		b.BrewingDay = r.FormValue("brewingday")
		b.OG, err = strconv.ParseFloat(r.FormValue("og"), 64)
		if err != nil {
			log.Print(err)
		}

		b.FG, err = strconv.ParseFloat(r.FormValue("fg"), 64)
		if err != nil {
			log.Print(err)
		}

		b.IBU, err = strconv.ParseFloat(r.FormValue("ibu"), 64)
		if err != nil {
			log.Print(err)
		}
		err = mysql.AddBrewing(&b)
		if err != nil {
			log.Print(err)
		}
		h.brewings(w, r)
	}

	rd := new(RequestData)
	rd.Title = "Bryggboken"
	rd.MenuItems = h.menu("/brewings/add")

	err := h.templates.ExecuteTemplate(w, "addbrewing.html", rd)
	if err != nil {
		log.Print("Cannot Get View ", err)
	}
}

func (h *Httpd) onTap(w http.ResponseWriter, r *http.Request) {
	rd := new(RequestData)
	rd.Title = "Bryggboken"
	rd.MenuItems = h.menu("/on-tap")

	err := h.templates.ExecuteTemplate(w, "on-tap.html", rd)
	if err != nil {
		log.Print("Cannot Get View ", err)
	}
}

func (h *Httpd) getTemplate(w http.ResponseWriter, r *http.Request) {
	uri := strings.TrimPrefix(r.RequestURI, "/")
	if uri == "" {
		h.brewings(w, r)
		return
	}

	folders := []string{".js", ".css", ".png", ".gif", "jpg"}
	for _, fld := range folders {
		if strings.HasSuffix(uri, fld) {
			http.ServeFile(w, r, h.root+"/"+uri)
			return
		}
	}

}

func (h *Httpd) logout(w http.ResponseWriter, r *http.Request) {
	if h.haveSession(r) {
		c, _ := r.Cookie("SESSION")
		h.removeSessionCookie(c, w)
		h.removeSession()
	}
	b, _ := json.Marshal(false)
	fmt.Fprint(w, string(b))
}

func (h *Httpd) loggedIn(w http.ResponseWriter, r *http.Request) {
	res := h.haveSession(r)
	b, _ := json.Marshal(res)
	fmt.Fprint(w, string(b))

}

func (h *Httpd) login(w http.ResponseWriter, r *http.Request) {
	if h.haveSession(r) {
		b, _ := json.Marshal(true)
		fmt.Fprint(w, string(b))
		return
	}
	if r.FormValue("password") == "changeme" && r.FormValue("username") == "admin" {
		uid := uuid.Must(uuid.NewV1())

		s := new(Session)
		s.GUID = uid.String()
		s.Timeout = new(time.Time)
		*s.Timeout = time.Now().Add(time.Hour * 1)
		h.Sessions = append(h.Sessions, s)
		cookie := &http.Cookie{Name: "SESSION", Expires: *s.Timeout, Value: s.GUID, HttpOnly: false}
		http.SetCookie(w, cookie)
		b, _ := json.Marshal(true)
		fmt.Fprint(w, string(b))
		return
	}

	b, _ := json.Marshal(false)
	fmt.Fprint(w, string(b))
}

func (h *Httpd) haveSession(r *http.Request) bool {
	c, err := r.Cookie("SESSION")
	if err != nil {
		return false
	}

	for _, s := range h.Sessions {
		if s.GUID == c.Value {
			return true
		}
	}
	return false
}

func (h *Httpd) updateSessionTimeout(cookie http.Cookie, w http.ResponseWriter) {
	for _, sess := range h.Sessions {
		if sess.GUID == cookie.Value {
			s := new(Session)
			s.GUID = sess.GUID
			s.Timeout = new(time.Time)
			*s.Timeout = time.Now().Add(time.Hour * 1)
			*sess.Timeout = *s.Timeout
			cookie := &http.Cookie{Name: "SESSION", Expires: *s.Timeout, Value: s.GUID, HttpOnly: false}
			http.SetCookie(w, cookie)
		}
	}
}

func (h *Httpd) removeSession() {
	for i, sess := range h.Sessions {
		if time.Now().After(*sess.Timeout) {
			h.Sessions = append(h.Sessions[:i], h.Sessions[i+1:]...)
		}
	}
}
func (h *Httpd) removeSessionCookie(cookie *http.Cookie, w http.ResponseWriter) {
	for _, sess := range h.Sessions {
		if sess.GUID == cookie.Value {
			s := new(Session)
			s.GUID = sess.GUID
			s.Timeout = new(time.Time)
			*s.Timeout = time.Now()
			*sess.Timeout = *s.Timeout
			cookie := &http.Cookie{Name: "SESSION", Expires: *s.Timeout, Value: s.GUID, HttpOnly: false}
			http.SetCookie(w, cookie)
		}
	}
}

func (h Httpd) unauthorized(w http.ResponseWriter) {
	w.WriteHeader(http.StatusUnauthorized)
	w.Write([]byte("401: Unauthorized"))

}

func (h Httpd) badRequest(w http.ResponseWriter) {
	w.WriteHeader(http.StatusBadRequest)
	w.Write([]byte("400: Bad Request"))
}

func (h Httpd) notFound(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNotFound)
	w.Write([]byte("404: Not Found"))
}

func (h Httpd) internalError(w http.ResponseWriter, err string) {
	w.WriteHeader(http.StatusInternalServerError)
	log.Println("500: Internal Server Error (" + err + ")")
	w.Write([]byte("500: Internal Server Error (" + err + ")"))
}
