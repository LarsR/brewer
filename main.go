package main

import (
	"flag"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/LarsR/brewer/database/mysql"
	"gitlab.com/LarsR/brewer/httpd"
)

func main() {

	templates := flag.String("template-path", os.Getenv("TEMPLATE-PATH"), "root where templates exists")
	dsn := flag.String("dsn", os.Getenv("DataSourceName"), "DataSourceName for database connection (user:password@host/database)")
	migrations := flag.String("migrations", os.Getenv("MIGRATIONS"), "Where are migrations found")
	flag.Parse()

	if os.Getenv("DB_USERNAME") != "" ||
		os.Getenv("DB_HOSTNAME") != "" ||
		os.Getenv("DB_DATABASE") != "" {
		*dsn = os.Getenv("DB_USERNAME") + ":" + os.Getenv("DB_PASSWORD") + "@" + os.Getenv("DB_HOSTNAME") + "/" + os.Getenv("DB_DATABASE")
	}
	if *dsn == "" {
		log.Fatal("Missing DataSourceName")
	}
	mysql.DataSourceName = *dsn

	if *templates == "" {
		*templates = "templates"
	}
	if *migrations == "" {
		*migrations = "migrations"
	}
	files, err := ioutil.ReadDir(PathToFullPath(*migrations))
	if err != nil {
		log.Fatal(err)
	}
	for _, file := range files {
		f := PathToFullPath(PathToFullPath(*migrations) + "/" + file.Name())
		err = mysql.Migrate(f)
		if err != nil {
			log.Fatal(err)
		}
	}

	httpd.New(PathToFullPath(*templates))

	sigterm := make(chan os.Signal, 1)
	signal.Notify(sigterm, syscall.SIGHUP, os.Kill, os.Interrupt, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGKILL)
	<-sigterm
}
